<!-- <style>
.btn{
    height: 24px;
    color: white;
    background: royalblue;
    border: none;
    border-radius: 5px;
}
.btn:hover{
    filter: brightness(0.9);
}
</style> -->

# Point Up v1.2 Third Party Transaction API

Authors:

- Aashish Gurung (ashish.gurung@cdts.com.np)
- Sudip Ghimire (sudip.ghimire@cdts.com.np)

## Contents
1. [Generating API Key](#generating-api-key)
2. [Listing/Updating API Key](#listingupdating-api-key)
3. [Listing out Cashiers](#listing-out-cashiers)
4. [Checking out Customer Balance](#checking-out-customer-balance)
5. [Creating Transaction](#creating-transaction)
6. [Completing the Transaction](#completing-the-transaction)
7. [Cancelling the Transaction](#cancelling-the-transaction)
8. [Listing Transactions](#listing-transactions)
9. [Retrieving the Transaction](#retrieving-the-transaction)
<hr>

## Generating API Key
In the Admin dashboard select the business for which you want to get an API access key and go to settings tab from sidebar. Click on the <button class="btn">API & Services</button> tab  and click on the <button class="btn">Generate</button> button. Clicking on the `Generate` button will open a form where you will need to enter the name for an API access key and then select the expiry date and time.

> *Note: Name for an API key must be unique with Business.<br>Expiry date and time is optional, if you do not select it, the lifetime of the API access key will be unlimited*

After filling out the form click on `create` button. It will create an API access key and show you the generated key to use the public APIs of PointUp.

> *Note: The key generated is onetime view key so after you have generated the key, make sure you save it locally or anywhere within your machine so that you can use it later to authorize the API usage.Only hashed version of key is viewable once you close the generated key from next time, This is for security purpose of the API access key.*

Following is the sample of the format of generated API access key:
`1ZfYtQU3.ZHgA7j8nEFw54PXSNgttZ030eotMVRuq`
<hr>


## Listing/Updating API Key
The created API access key will be visible in the `API & Services` tab automatically with information of creation date, expiry date and revoked status. 

To `Update` the expiry date of a key that do not have any expiry date click on the action menu and then click on the `Add expiry date` button, It will open a model from where you can update the expiry date.

To make an API key **`Inactive`** you can click on the action menu and then click on the `Revoke` button.<br>

> *Note: You cannot `UnRevoke` the API access key once you have revoked it. If you need a key you will have to regenerate it for that business with different API key name.*

***
## Listing Out Cashiers
The URL for listing out Cashiers is https://pointup.com.my/api/public/business/user/. to list out cashiers we have to provide api key in the `Authorization` part of the `header` section with format of `Api-Key <key>`. If you do not have api key, please refer to
[Generating API Key](##-Generating-API-Key) section.

### <ins>Request Description</ins>

| SN | title | Description|
|---|---|---|
| 1 | URL | https://pointup.com.my/api/public/business/user/ |
| 2 | Method | `GET` |
| 3 | Headers| `Authorization Api-Key <api_key>` |
| 4 | Success Code | `200 Ok` |
| 5 | Error Codes | `400 Bad Request`, `401 Unauthorized` |


### cURL Example
we can use the following command for fetching out the customers

``` shell
curl --location --request GET 'https://pointup.com.my/api/public/business/user/' --header 'Authorization: Api-Key 1ZfYtQU3.ZHgA7j8nEFw54PXSNgttZ030eotMVRuq'
```

### Javascript Example

``` js
var myHeaders = new Headers();
myHeaders.append("Authorization", "Api-Key 1ZfYtQU3.ZHgA7j8nEFw54PXSNgttZ030eotMVRuq");

var requestOptions = {
  method: 'GET',
  headers: myHeaders,
  redirect: 'follow'
};

fetch("https://pointup.com.my/api/public/business/user/", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
```

### Output

1. **Successful Response**

    On Success, it returns the json response from the server similar to the following format.
    ``` json
    {
        "links": {
            "next": null,
            "previous": null
        },
        "total": 5,
        "offset": null,
        "results": [
            {
                "id": 2,
                "business": 2,
                "is_admin": true,
                "is_owner": true,
                "is_agent": true,
                "is_operator": true,
                "user": {
                    "id": 2,
                    "first_name": "John",
                    "last_name": "Doe",
                    "email": "johndoe@email.com",
                    "phone": "1234567890",
                    "address": "",
                    "image": null,
                    "username": null
                },
                "is_active": true
            },
            {
                "id": 7,
                "business": 2,
                "is_admin": false,
                "is_owner": false,
                "is_agent": true,
                "is_operator": false,
                "user": {
                    "id": 6,
                    "first_name": "Admin",
                    "last_name": "One",
                    "email": "admin1@email.com",
                    "phone": "9771234567890",
                    "address": "",
                    "image": null,
                    "username": null
                },
                "is_active": true
            },
            {
                "id": 8,
                "business": 2,
                "is_admin": false,
                "is_owner": false,
                "is_agent": true,
                "is_operator": false,
                "user": {
                    "id": 7,
                    "first_name": "Admin",
                    "last_name": "Two",
                    "email": "admin2@email.com",
                    "phone": "9771234567890",
                    "address": "",
                    "image": null,
                    "username": null
                },
                "is_active": true
            },
            {
                "id": 9,
                "business": 2,
                "is_admin": false,
                "is_owner": false,
                "is_agent": false,
                "is_operator": true,
                "user": {
                    "id": 8,
                    "first_name": "Admin",
                    "last_name": "Three",
                    "email": "admin3@email.com",
                    "phone": "9770987654321",
                    "address": "",
                    "image": null,
                    "username": null
                },
                "is_active": true
            },
            {
                "id": 11,
                "business": 2,
                "is_admin": false,
                "is_owner": false,
                "is_agent": true,
                "is_operator": true,
                "user": {
                    "id": 9,
                    "first_name": "admin",
                    "last_name": "four",
                    "email": "admin4@email.com",
                    "phone": "9777896541230",
                    "address": "",
                    "image": null,
                    "username": null
                },
                "is_active": true
            }
        ]
    }
    ```

2. Unsuccessful Response

If we do not provide `Api-Key` in the header section, we will get the following error with the response code of `401 Unauthorized`
``` json
{
    "detail": "Authentication credentials were not provided."
}
```
<br>


## Checking Out Customer Balance
The URL for checking out customer balance is [https://pointup.com.my/api/public/customer_balance/<user_id>/](https://pointup.com.my/api/public/customer_balance/<user_id>/). To check out the token balance of a particular customer provide api key in the `Authorization` part of the `header` section with format of `Api-Key <key>`. If you do not have api key, please refer to
[Generating API Key](##-Generating-API-Key) section.


<br>

### <ins>Request Description</ins>

| SN | title | Description|
|---|---|---|
| 1 | URL | https://pointup.com.my/api/public/customer_balance/59/ |
| 2 | Method | `GET` |
| 3 | Headers| `Authorization Api-Key <api_key>` |
| 4 | Success Code | `200 Ok` |
| 5 | Error Codes | `400 Bad Request`, `401 Unauthorized` |

### cURL Example
we can use the following command for fetching out the customers
``` shell
curl --location --request GET 'https://pointup.com.my/api/public/customer_balance/59/' --header 'Authorization: Api-Key sF8eSGT5.vFRMgNeJXfSDf1HnLFNxcoQplGRJFOOe'
```
### Javascript Example

``` js
var myHeaders = new Headers();
myHeaders.append("Authorization", "Api-Key sF8eSGT5.vFRMgNeJXfSDf1HnLFNxcoQplGRJFOOe");

var requestOptions = {
  method: 'GET',
  headers: myHeaders,
  redirect: 'follow'
};

fetch("https://pointup.com.my/api/public/customer_balance/59/", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
```

### Output
``` json
{
    "links": {
        "next": null,
        "previous": null
    },
    "total": 4,
    "offset": null,
    "results": [
        {
            "id": 9,
            "quantity": 38,
            "balance": 38,
            "token_type": "Cash Token",
            "name": "Cool Cash Token"
        },
        {
            "id": 1,
            "quantity": 75,
            "balance": 69,
            "token_type": "Cash Token",
            "name": "TK1"
        },
        {
            "id": 2,
            "quantity": 1,
            "balance": 1,
            "token_type": "Voucher",
            "name": "Promo 1"
        },
        {
            "id": 3,
            "quantity": 1,
            "balance": 1,
            "token_type": "Coupon",
            "name": "C1"
        }
    ]
}

```

## Creating Transaction

Transaction creation process includes the tokens to be consumed and rewarded tokens list.  This requires unique `invoice_no` of the same business.

The url for creating transaction is https://pointup.com.my/api/public/transaction/

| SN | title | Description|
|---|---|---|
| 1 | URL | https://pointup.com.my/api/public/transaction/ |
| 2 | Method | `POST` |
| 3 | Headers| `Authorization Api-Key <api_key>` |
| 4 | Success Code | `201 Created` |
| 5 | Error Codes | `400 Bad Request`, `401 Unauthorized` |
| 6 | Content Type | `Application/json` or `multipart/form-data` |

**JSON Body Example**
``` json
{
    "invoice_no": "INV_USB_SMTP",
    "details": "",
    "amount": 450,
    "user": 333,
    "cashier": 2,
    "user_tokens": [
        {
            "user_token": 1,
            "quantity": 1,
            "redeem_code": "WVmpyDNH",
            "amount": 10
        }
    ],
    "reward_tokens": [
        {
            "issued_token": 1,
            "quantity": 1,
            "user": 3
        }
    ]
}
```

**Note**: 
> Here, `user_tokens` key can have empty array if customer does not want to spend any tokens
> and also `reward_tokens` key can have empty array if an operator or cashier does not want to reward any tokens during the transaction.



### cURL Example
``` shell
curl --location --request POST 'https://pointup.com.my/api/public/transaction/' \
--header 'Authorization: Api-Key 5uE8eM4M.fqtQV5kXiLBR0DJsfVb27nnXLIizaLX1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "invoice_no": "INV_USB_SMTP",
    "details": "",
    "amount": 450,
    "user": 333,
    "cashier": 2,
    "user_tokens": [
        {
            "user_token": 1,
            "quantity": 1,
            "redeem_code": "WVmpyDNH",
            "amount": 10
        }
    ],
    "reward_tokens": [
		{
            "issued_token": 1,
            "quantity": 1,
            "user": 3
        }
    ]
}'
```

### Javascript Example

```js
var myHeaders = new Headers();
myHeaders.append("Authorization", "Api-Key 5uE8eM4M.fqtQV5kXiLBR0DJsfVb27nnXLIizaLX1");
myHeaders.append("Content-Type", "application/json");

var raw = JSON.stringify({"invoice_no":"INV_RAM_THX","details":"","amount":450,"user":333,"cashier":2,"user_tokens":[{"token_type":"Cash Token","user_token":1,"quantity":1,"redeem_code":"WVmpyDNH","amount":10}],"reward_tokens":[{"issued_token": 1,"quantity": 1,"user": 3}]});

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: raw,
  redirect: 'follow'
};

fetch("https://pointup.com.my/api/public/transaction/", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));

```

### Output
```json
{
    "code": "OK",
    "message": "Transaction created successfully",
    "data": {
        "id": 18,
        "date": "2020-11-09 13:19:04",
        "status": "created",
        "is_returned": false,
        "valid_upto": "2020-11-12",
        "returnable": true,
        "invoice_no": "INV_IB_AGP",
        "user": 3,
        "pos": "Test 1604560082 pos",
        "business": {
            "id": 2,
            "name": "John Tech",
            "category": 6,
            "address": "TU Rd, Kirtipur",
            "address_line_2": null,
            "description": "abc",
            "website": "https://johntech.com",
            "email": "info@johntech.com",
            "phone_no": "+977 1234567890",
            "logo": "/media/media/images/business/2020-07-20/logo256.png",
            "latitude": 27.683475108269068,
            "longitude": 85.30681549484743
        },
        "agent": null,
        "operator": "johndoe@email.com",
        "details": "",
        "amount": 450.0,
        "total_paid": 440.0,
        "earned_points": 4.4,
        "redeemed_points": 0.0,
        "redeemed_amount": 10.0,
        "redeemed_tokens": [
            {
                "user_token_id": 1,
                "promo_name": null,
                "quantity": 1,
                "token_name": "TK1",
                "amount": 10.0,
                "revertible": true
            }
        ],
        "reward_tokens": [
            {
                "id": 10,
                "quantity": 1,
                "token_name": "Token 1",
                "revertible": true
            }
        ]
    }
}
```
<br><br>

## Completing The Transaction

Completing the transaction is the process of confirming the transaction which is available only when using the public api or the API KEY authenticated transaction.
We need either **transaction id** or **invoice number** for completing the transaction.
If we pass both the parameters then our API will look into transaction id parameter.

The url for completing the transaction is https://pointup.com.my/api/public/transaction/complete/ 

**<ins>Request Description</ins>**

| SN | title | Description|
|---|---|---|
| 1 | URL | https://pointup.com.my/api/public/transaction/complete/ |
| 2 | Method | `POST` |
| 3 | Headers| `Authorization Api-Key <api_key>` |
| 4 | Success Code | `200 OK` |
| 5 | Error Codes | `400 Bad Request`,<br> `401 Unauthorized`,<br> `404 UNKNOWN_TRANSACTION`<br> |
| 6 | Content Type | `Application/json` or `multipart/form-data` |
| 7 | API Error Codes |  `UNKNOWN_TRANSACTION`,<br> `NO_TRANSACTION_SPECIFIED`,<br> `UNAUTHORIZED`,<br> `ALREADY_CANCELLED`,<br> `ALREADY_COMPLETED` |

<br>

### cURL Example With `invoice_no`
``` shell
curl --location --request POST 'https://pointup.com.my/api/public/transaction/complete/' --header 'Authorization: Api-Key 5uE8eM4M.fqtQV5kXiLBR0DJsfVb27nnXLIizaLX1' --form 'invoice_no=INV_XML_XSS'

```


### cURL Example With transaction `id`
``` shell
curl --location --request POST 'https://pointup.com.my/api/public/transaction/complete/' --header 'Authorization: Api-Key 5uE8eM4M.fqtQV5kXiLBR0DJsfVb27nnXLIizaLX1' --form 'id=100'

```

### Javascript Example
``` js
var myHeaders = new Headers();
myHeaders.append("Authorization", "Api-Key 5uE8eM4M.fqtQV5kXiLBR0DJsfVb27nnXLIizaLX1");

var formdata = new FormData();
formdata.append("invoice_no", "INV_XML_XSS");

// or With ID, we can do the following
// formdata.append("id", "100");

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: formdata,
  redirect: 'follow'
};

fetch("https://pointup.com.my/api/public/transaction/complete/", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
```


### Outputs
1. Success
    ``` json
    {
        "code": "OK",
        "Message": "Transaction Complete"
    }
    ```

2. Error `ALREADY COMPLETED`
    ``` json
    {
        "code": "ALREADY_COMPLETED",
        "detail": "This transaction has already been completed and hence can not be completed"
    }
    ```
3. Error `UNKNOWN_TRANSACTION`
    ```json
    {
        "code": "UNKNOWN_TRANSACTION",
        "detail": {
            "invoice_no": [
            "Transaction with invoice_no INV_XML_XSS does not exist"
            ]
        }
    }
    ```

<br><br>

## Cancelling The Transaction

Cancelling the transaction is the process of confirming the transaction which is available only when using the public api or the API KEY authenticated transaction.
We need either **transaction id** or **invoice number** for cancelling the transaction. The cancellation process is similar to the completion process.

The url for cancelling the transaction is https://pointup.com.my/api/public/transaction/cancel/


| SN | title | Description|
|---|---|---|
| 1 | URL | https://pointup.com.my/api/public/transaction/cancel/ |
| 2 | Method | `POST` |
| 3 | Headers| `Authorization Api-Key <api_key>` |
| 4 | Success Code | `200 OK` |
| 5 | Error Codes | `400 Bad Request`, `401 Unauthorized`, `404 UNKNOWN_TRANSACTION` |
| 6 | Content Type | `Application/json` or `multipart/form-data` |
| 7 | API Error Codes |  `UNKNOWN_TRANSACTION`,<br> `NO_TRANSACTION_SPECIFIED`,<br> `UNAUTHORIZED`,<br> `ALREADY_CANCELLED`,<br> `ALREADY_COMPLETED` |
 

### cURL Example
``` shell
curl --location --request POST 'https://pointup.com.my/api/public/transaction/cancel/' --header 'Authorization: Api-Key 5uE8eM4M.fqtQV5kXiLBR0DJsfVb27nnXLIizaLX1' --form 'invoice_no=INV_XML_XSS'
```

if we want to cancel transaction by `id`, we have to pass the `id` instead of `invoice_no` in the `--form` argument

example:

``` shell
curl --location --request POST 'https://pointup.com.my/api/public/transaction/cancel/' --header 'Authorization: Api-Key 5uE8eM4M.fqtQV5kXiLBR0DJsfVb27nnXLIizaLX1' --form 'id=100'
```



### Javascript Example
``` js
var myHeaders = new Headers();
myHeaders.append("Authorization", "Api-Key 5uE8eM4M.fqtQV5kXiLBR0DJsfVb27nnXLIizaLX1");

var formdata = new FormData();
formdata.append("invoice_no", "INV_XML_XSS");

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: formdata,
  redirect: 'follow'
};

fetch("https://pointup.com.my/api/public/transaction/cancel/", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
```


### Output
1. success
    ``` json
    {
        "code": "OK",
        "Message": "Transaction Cancelled"
    }
    ```
2. Error ( `ALREADY_CANCELLED` )
    ```json
    {
        "code": "ALREADY_CANCELLED",
        "detail": "This transaction has already been cancelled and hence can not be cancelled"
    }
    ```

3. Error ( `UNKNOWN_TRANSACTION` )
    ```json
        {
            "code": "UNKNOWN_TRANSACTION",
            "detail": {
                "invoice_no": [
                    "Transaction with invoice_no INV_IB_IB does not exist"
                ]
            }
        }
    ```
4. Error (`UNKNOWN_TRANSACTION`) when `id` passed
    ```json
    {
        "code": "UNKNOWN_TRANSACTION",
        "detail": {
            "id": [
                "Transaction with id 100 does not exist"
            ]
        }
    }
    ```

<br><br>



## Listing Transactions

The url for listing the transaction is https://pointup.com.my/api/public/transaction/

| SN | title | Description|
|---|---|---|
| 1 | URL | https://pointup.com.my/api/public/transaction/ |
| 2 | Method | `GET` |
| 3 | Headers| `Authorization Api-Key <api_key>` |
| 4 | Success Code | `200 OK` |
| 5 | Error Codes |`401 Unauthorized`|
| 6 | Content Type | `Application/json` or `multipart/form-data` |

### cURL Example

``` shell
curl --location --request GET 'https://pointup.com.my/api/public/transaction/' --header 'Authorization: Api-Key sF8eSGT5.vFRMgNeJXfSDf1HnLFNxcoQplGRJFOOe' --header 'Content-Type: application/json'
```

### Javascript Example

```js
var myHeaders = new Headers();
myHeaders.append("Authorization", "Api-Key sF8eSGT5.vFRMgNeJXfSDf1HnLFNxcoQplGRJFOOe");
myHeaders.append("Content-Type", "application/json");
var requestOptions = {
  method: 'GET',
  headers: myHeaders
};

fetch("https://pointup.com.my/api/public/transaction/", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));

```

### Output
``` json
{
    "links": {
        "next": null,
        "previous": null
    },
    "total": 2,
    "offset": null,
    "results": [
        {
            "id": 3,
            "user_tokens": [
                {
                    "id": 1,
                    "promo_id": null,
                    "quantity": 1,
                    "redeem_code": "JI7EsmaF",
                    "token_name": "test"
                }
            ],
            "pos": 11,
            "date": "2020-11-08 12:57:52",
            "amount": 450.0,
            "agent": null,
            "details": "",
            "status": "created"
        },
        {
            "id": 2,
            "user_tokens": [
                {
                    "id": 1,
                    "promo_id": null,
                    "quantity": 0,
                    "redeem_code": "JI7EsmaF",
                    "token_name": "test"
                }
            ],
            "pos": 11,
            "date": "2020-11-08 11:55:23",
            "amount": 450.0,
            "agent": null,
            "details": "",
            "status": "cancelled"
        }
    ]
}
```


<br><br>


## Retrieving the Transaction

The url for retrieving transaction is https://pointup.com.my/api/public/transaction/detail/transaction_id/ 

| SN | title | Description|
|---|---|---|
| 1 | URL | https://pointup.com.my/api/public/transaction/detail/transaction_id/ |
| 2 | Method | `GET` |
| 3 | Headers| `Authorization Api-Key <api_key>` |
| 4 | Success Code | `200 OK` |
| 5 | Error Codes | `400 Bad Request`, `401 Unauthorized`, `404 Not Found` |
| 6 | Content Type | `Application/json` or `multipart/form-data` |
| 7 | API Error Codes |  `UNKNOWN_TRANSACTION`, |

### cURL Example

``` shell
curl --location --request GET 'https://pointup.com.my/api/public/transaction/detail/3/' --header 'Authorization: Api-Key sF8eSGT5.vFRMgNeJXfSDf1HnLFNxcoQplGRJFOOe'
```

### Javascript Example

```js
var myHeaders = new Headers();
myHeaders.append("Authorization", "Api-Key sF8eSGT5.vFRMgNeJXfSDf1HnLFNxcoQplGRJFOOe");
var requestOptions = {
  method: 'GET',
  headers: myHeaders,
  redirect: 'follow'
};

fetch("https://pointup.com.my/api/public/transaction/detail/3/", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));

```

### Output
``` json
{
    "code": "OK",
    "data": {
        "id": 3,
        "date": "2020-11-08 12:57:52",
        "status": "created",
        "is_returned": false,
        "valid_upto": "2020-11-11",
        "returnable": true,
        "invoice_no": "INV_SQL_JBOD",
        "user": 59,
        "pos": "Intel pos",
        "business": {
            "id": 6,
            "name": "test business",
            "category": 6,
            "address": "test",
            "address_line_2": null,
            "description": "test",
            "website": "https://testbusiness.com",
            "email": "info@testbusiness.com",
            "phone_no": "+7 79845620776",
            "logo": "/media/media/images/business/2020-08-25/118516690_1192956531086565_5864129799989603464_n.jpg",
            "latitude": 27.710342132533572,
            "longitude": 85.31777220104777
        },
        "agent": null,
        "operator": "testoperator@email.com",
        "details": "",
        "amount": 450.0,
        "total_paid": 0.0,
        "earned_points": 0.0,
        "redeemed_points": 0.0,
        "redeemed_amount": 450.0,
        "redeemed_tokens": [
            {
                "user_token_id": 1,
                "promo_name": null,
                "quantity": 1,
                "token_name": "test",
                "amount": 450.0,
                "revertible": true
            }
        ],
        "reward_tokens": []
    },
    "message": "Transaction retrieve successful"
}
```
## Unsuccessful Response

If we do not provide `Api-Key` in the header section, we will get the following error with the response code of `401 Unauthorized`
``` json
{
    "detail": "Authentication credentials were not provided."
}
```

If we request the API with the transaction id that does not exists in our system then we will get the following error with the code `404 Not Found`

``` json
{
    "code": "UNKNOWN_TRANSACTION",
    "detail": "Transaction with the provided Id does not exist."
}
```